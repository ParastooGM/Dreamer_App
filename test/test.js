const request = require("supertest");
const { expect } = require("expect");
const { app } = require("../server");
const Item = require("../models/Item");
var db_item;
var api_item;

describe("Testing POST /api/users endpoint with an already existing user.", () => {
  it("respond with valid HTTP status code and description and message.", async () => {
    const response = await request(app).post("/api/users").send({
      name: process.env.DEFAULT_NAME,
      email: process.env.DEFAULT_EMAIL,
      password: process.env.DEFAULT_PASSWORD,
    });

    expect(response.status).toBe(400);
    expect(response.body.msg).toBe("User already exists.");
  });
});

describe("Testing POST /api/users/auth endpoint.", () => {
  it("respond with valid HTTP status code and description and message", async () => {
    const response = await request(app).post("/api/users/auth").send({
      email: process.env.DEFAULT_EMAIL,
      password: process.env.DEFAULT_PASSWORD,
    });

    expect(response.status).toBe(200);
  });
});

describe("Testing saving an Item to the database.", function () {
  it("should save without error", function (done) {
    var item = new Item({
      title: "I was running in the forrest.",
      content:
        "I had a dream last night where I I was running in the forrest at night and animals were following me. ",
    });
    db_item = item;
    item.save(function (err) {
      if (err) done(err);
      else done();
    });
  });
});

describe("Testing deleting an Item from the database.", function () {
  it("should delete without error", function (done) {
    db_item.remove(function (err) {
      if (err) done(err);
      else done();
    });
  });
});

describe("Testing POST /api/items endpoint with authentication", () => {
  it("respond with valid HTTP status code and description and message", async () => {
    const res = await request(app).post("/api/users/auth").send({
      email: process.env.DEFAULT_EMAIL,
      password: process.env.DEFAULT_PASSWORD,
    });

    const response = await request(app)
      .post("/api/items")
      .set({ "x-auth-token": res.body.token })
      .send({
        title: "I was flying!",
        content:
          "I had a dream last night where I had wings and I was flying. ",
      });

    api_item = response.body._id.toString();
    expect(response.status).toBe(200);
    expect(response.body.title).toBe("I was flying!");
  });
});

describe("Testing DELETE /api/items endpoint with authentication", () => {
  it("respond with valid HTTP status code and description and message", async () => {
    const res = await request(app).post("/api/users/auth").send({
      email: process.env.DEFAULT_EMAIL,
      password: process.env.DEFAULT_PASSWORD,
    });

    const response = await request(app)
      .delete("/api/items/" + api_item)
      .set({ "x-auth-token": res.body.token });

    expect(response.status).toBe(200);
  });
});

describe("Testing POST /api/items endpoint without authentication", () => {
  it("respond with valid HTTP status code and description and message", async () => {
    const response = await request(app).post("/api/items").send({
      title: "I was flying!",
      content: "I had a dream last night where I had wings and I was flying. ",
    });

    expect(response.status).toBe(401);
    expect(response.body.msg).toBe("User unauthorozied, no token.");
  });
});
