This is a dockerized web application created with the MERN stack (MongoDB, Express, React, and Node JS), and JWT authentication. It is a dream journal where each dream's title can be visualized using OpenAI's image generation API.

Currently deployed on https://dreamer-4qe5.onrender.com
